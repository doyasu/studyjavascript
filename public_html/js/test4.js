// ５章


/*
 * new式によるコンストラクタの呼び出し
 */
function MyClass1(x, y){
    this.x = x;
    this.y = y;
}

function constractor1(){

    var obj = new MyClass1(3, 2);
    print('MyClass1 Object : ' + obj.x + ',' + obj.y);

}

/*
 * メソッド（っぽく）記述できる
 * メソッド定義の実態をオブジェクトごとに持つのでメモリ効率、実行効率が悪い
 */
function MyClass2(x, y){
    this.x = x;
    this.y = y;

    this.show = function(){
        print('MyClass2 Object : ' + this.x + ',' + this.y);
    };
}

function constractor2(){
    var obj = new MyClass2(3, 2);
    obj.show();
}

/*
 * プロトタイプ継承
 * MyClass3コンストラクタで生成したオブジェクトは、MyClass3.prototypeオブジェクトのプロパティを共有
 *
 * ・全てのオブジェクトはprototypeプロパティを持つ
 * ・全てのオブジェクトはオブジェクト生成に使ったコンストラクタのprototypeオブジェクトへの暗黙リンクを持つ
 *
 * プロトタイプチェーン - オブジェクトのプロパティ読み込みは、次の順でプロパティを探す
 * 1. オブジェクト自身のプロパティ
 * 2. 暗黙リンクの参照オブジェクト（コンストラクタのprototypeオブジェクト）のプロパティ
 * 3. 2のオブジェクトの暗黙リンクの参照オブジェクトのプロパティ
 * 4. 探索の終端（Object.prototypeオブジェクト）まで3を繰り返す
 */
function MyClass3(x, y){
    this.x = x;
    this.y = y;
}
MyClass3.prototype.show = function(){
    print('MyClass3 Object : ' + this.x + ',' + this.y);
};

function constractor3(){
    var obj = new MyClass3(3, 2);
    obj.show();
}

/*
 * プロパティの属性を設定する関数
 * Object.preventExtensions : プロパティの追加を禁止
 * Object.seal              : プロパティの追加・削除を禁止
 * Object.freeze            : プロパティの追加・削除・変更を禁止
 */

 function propertyAccess0(){
     var obj = { x:2, y:3 };

     print('default');

     obj.z = 4;
     print(' - add property : Object.keys = ' + Object.keys(obj));
     delete obj.y;
     print(' - delete property : Object.keys = ' + Object.keys(obj));
     obj.x = 20;
     print(' - edit property : obj.x = ' + obj.x);

 }

function propertyAccess1(){
    var obj = { x:2, y:3 };
    Object.preventExtensions(obj);

    print('preventExtensions');

    obj.z = 4;
    print(' - add property : Object.keys = ' + Object.keys(obj));
    delete obj.y;
    print(' - delete property : Object.keys = ' + Object.keys(obj));
    obj.x = 20;
    print(' - edit property : obj.x = ' + obj.x);

}

function propertyAccess2(){
    var obj = { x:2, y:3 };
    Object.seal(obj);

    print('seal');

    obj.z = 4;
    print(' - add property : Object.keys = ' + Object.keys(obj));
    delete obj.y;
    print(' - delete property : Object.keys = ' + Object.keys(obj));
    obj.x = 20;
    print(' - edit property : obj.x = ' + obj.x);

}

function propertyAccess3(){
    var obj = { x:2, y:3 };
    Object.freeze(obj);

    print('freeze');

    obj.z = 4;
    print(' - add property : Object.keys = ' + Object.keys(obj));
    delete obj.y;
    print(' - delete property : Object.keys = ' + Object.keys(obj));
    obj.x = 20;
    print(' - edit property : obj.x = ' + obj.x);

}

/*
 * this参照の規則
 * 関数内のthis参照は関数の呼び出し方法で異なる
 *
 * ・コンストラクタ呼び出し  : 生成したオブジェクト
 * ・メソッド呼び出し       : レシーバオブジェクト
 * ・apply / call呼び出し  : apply / callで指定したオブジェクト
 * ・それ以外の呼び出し     : グローバルオブジェクト
 */
var x = 'global x';
function thisIsYabai(){
    var obj = {
        x: 'obj.property x',
        doit: function() { print('doit method : this.x = ' + this.x); }
    };
    // レシーバオブジェクトはobj
    obj.doit(); // メソッド呼び出し

    var fn = obj.doit;
    fn();       // それ以外の呼び出し

    var obj2 = {
        x: 'obj2.property x',
        doit2: fn
    };
    // レシーバオブジェクトはobj2
    obj2.doit2();   // メソッド呼び出し

    var obj3 = { x:'obj3.property x'};
    fn.apply(obj);  // apply呼び出し（第２引数の配列要素がfnの引数になる）
    fn.call(obj3);  // cally呼び出し（第２引数以降の引数がfnの引数になる）
}
