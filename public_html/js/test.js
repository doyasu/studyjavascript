// 関数はオブジェクト
function gerogero(){
  var oee = 'oee';
  document.write(oee);
  document.write("<br>");
}

// オブジェクトリテラル式でオブジェクトの生成
// ドット演算でプロパティにアクセス
function gerogero2(){
  var obj = { oee:"oeeoee", gero:"gerogero" };
  document.write(obj.oee);
  document.write("<br>");
  document.write(obj.gero);
  document.write("<br>");
}

// 存在しないプロパティ名に値を代入すると
// 新規プロパティが追加されます(!??!????)
// obj.oee
function gerogero3(){
  var obj = { oee:"oeeoeee" };
  obj.gero = "gerogero";

  document.write(obj.gero);
  document.write("<br>");
}
