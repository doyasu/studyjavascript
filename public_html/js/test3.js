
/*
 * 識別子はUnicodeなら何でもいい
 * 非推奨
 */
function 関数(){
  var 変数 = "識別子はUnicodeなら一応OK";
  print(変数);
}

/*
 * 改行時に省略されたセミコロンを補完する機能がある
 * バグの温床なので非推奨
 */
function semicolon(){
  var semi = ';を補完するバグの温床'
  print(semi)
}

/*
 * 式文の仕様
 * 意味のないコードはコンパイルエラーにならないため
 * = と == を書き間違えてもわかりません
 */
function 式文(){
  var a;

  a == 0;
  print("a == 0;はコンパイルエラーにならない");
}

/*
 * jsのswitch文は、===で型変換なしの同値比較する
 * case文はJavaと違い、任意の式を記述できる
 */
function switchState(){
  var s = new String('foo');
  var s2 = new String('foo');

  switch(s){
    case 0:
      print('s is 0');
      break;
    case s2:
      print('s = s2');
      break;
    case 'foo':
      print('s = foo');
      break;
    default:
      // 型変換なしのためdefaultが実行される
      print('型変換なしの === で同値比較される');
      break;
  }
}

/*
 * for in文
 * オブジェクトのプロパティを列挙するが、順序は保証されない
 * 配列も順序が保証されない
 * 属性のプロパティは列挙されない（例：文字列型.length）
 */
function forInState(){
  var obj = { x:1, y:2, z:3 };
  var arr = [7, 1, 5];
  var boolObj = new Boolean();

  // プロパティ名を表示
  for (var p in obj){
    print(p);
  }
  // プロパティの値を表示
  for (var v in obj){
    print(obj[v]);
  }

  // 配列を表示
  for (var a in arr){
    print(arr[a]);
  }

  // 属性のプロパティは列挙できない
  for (var b in boolObj){
    print(b);
  }
  print('属性のプロパティは列挙できない');

  // 本来のBooleanオブジェクトのプロパティ
  var pBool = listAllProperties(boolObj);
  print(pBool);
}

/*
 * オブジェクトの全プロパティをリストして文字列を返す
 * 本来列挙不可なプロパティを参照するための関数
 */
function listAllProperties(o){
	var objectToInspect;
	var result = [];

	for(objectToInspect = o; objectToInspect !== null; objectToInspect = Object.getPrototypeOf(objectToInspect)){
		result = result.concat(Object.getOwnPropertyNames(objectToInspect));
	}

	return result;
}

/*
 * throw文で例外オブジェクトを投げる
 * throw文に任意の型の式を書ける
 */
function throwState(){
  throw 'throw';
}

/*
 * catchは１つしか書けない
 */
function tryCatch(){
  try {
    throwState();
  } catch(e){
    print(e);
    print('catch');
  } finally {
    print('finally');
  }
}
