var print = function(str){
  document.write(str);
  document.write("<br>");
}

/*
 * jsの型は6種類
 * 文字列型、数値型、ブーリアン型、null型、undefined型、オブジェクト型
 * 関数はオブジェクト型だが、特別な扱い
 * typeof nullがnullではなくobjectとなるのは仕様
 * 仕様だからしょうがないね
 */
function typeDefine(){
  var str = "string";
  var num = 1;
  var bool = true;
  var undef;
  var obj = {};
  var func = function(){};

  var result;

  result = typeof str;
  print("str = " + result);

  result = typeof num;
  print("num = " + result);

  result = typeof bool;
  print("bool = " + result);

  result = typeof null;
  print("null = " + result);

  result = typeof undef;
  print("undef = " + result);

  result = typeof obj;
  print("obj = " + result);

  result = typeof func;
  print("func = " + result);
}

/*
 * ==は暗黙の型変換をして同値判定
 * ===は型変換せずに同値判定
 */
function equal(){
  var numObj1 = new Number(1);
  var numObj2 = new Number(1);

  // 参照先オブジェクトが異なるのでfalse
  if(numObj1 == numObj2){
    print("numObj1 == numObj2 : true");
  } else {
    print("numObj1 == numObj2 : false");
  }

  // 暗黙の型変換をするためtrue
  if(numObj1 == 1){
    print("numObj1 == 1 : true");
  } else {
    print("numObj1 == 1 : false");
  }

  // 型変換なし、オブジェクトと数値の比較のためfalse
  if(numObj1 === 1){
    print("numObj1 === 1 : true");
  } else {
    print("numObj1 === 1 : false");
  }
}

/*
 * NaNの取り扱いに注意
 */
function notANumber(){
  // 数値に型変換できない値の場合NaNとなる
  var notNumber = new Number('10x');
  print("notNumber : " + notNumber);

  // parseInt()またはparseFloat()は数値以外無視してくれる
  var number = parseInt('10x');
  print("number parseInt('10x') = " + number);

  // 何を演算してもNaNのまま変わらない
  notNumber++;
  print("notNumber++ : " + notNumber);

  // NaNかどうかの判定はisNaN()を用いること
  if(isNaN(notNumber)){
    print("isNum() : true");
  } else {
    print("isNum() : false");
  }

  // NaNはNaNじゃないのでfalse
  if(NaN == NaN){
    print("NaN == NaN : true");
  } else {
    print("NaN == NaN : false");
  }

}

/*
 * 下記の値以外はすべて型変換でtrue
 * 数値0, 数値NaN, null値, undefined値, 空文字列値""
 */
function typeBoolean(){
  // オブジェクト型は型変換でtrueとなる
  var b = new Boolean(false);
  if(b){
    print("new Boolean(false) : true");
  } else {
    print("new Boolean(false) : false");
  }

  // Booleanクラスのオブジェクトではなく
  // 組み込み型のboolean型として扱われるためfalse
  var b = Boolean(false);
  if(b){
    print("Boolean(false) : true");
  } else {
    print("Boolean(false) : false");
  }
}
